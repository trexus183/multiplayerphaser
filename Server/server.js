/* jshint esversion: 6 */
const http = require('http'); 
const uuid = require('uuid/v4');
const localHostname = '0.0.0.0';
const localPort = 8080;
const herokuPort = process.env.PORT || 3000;

const server = http.createServer(function (req, res) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.setHeader('Access-Control-Allow-Origin', '*'); //TODO Replace localhost with actual host
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, PATCH, POST, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    if (req.url == '/resetServer') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("Server reset");
        players = [];
    } else if (req.url == '/getNewId') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        var newId = uuid();
        res.end(newId);
        console.log("User connected - ID: " + newId);
    } else if (req.url == '/updatePlayerVals') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end(JSON.stringify(connectedPlayers));

        let body = [];
        req.on ('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();

            if (body != '') {
                newPlayerVals = JSON.parse(body);

                for (var i = 0; i < connectedPlayers.length; i++) {
                    if (connectedPlayers[i].id == newPlayerVals.id) {
                        connectedPlayers[i] = newPlayerVals;
                        return;
                    }
                }

                connectedPlayers.push(newPlayerVals);
                console.log("Added new player");
            }
        });
    } else if (req.url == '/ping') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('pong');
    } else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end("SERVER FAILED TO RECOGNIZE CALL URL");
    }
});

server.listen(localPort, localHostname, function () {
    console.log('Server running at http://' + localHostname + ':' + localPort + '/');

    serverTick();
});

// server.listen(herokuPort, () => {
//     console.log(`Server is running on port ${herokuPort}`);

//     serverTick();
// });

var connectedPlayers = [];

lastUpdate = Date.now();

function serverTick () {
    timeSinceLastTick = Date.now() - lastUpdate;

    // console.log(timeSinceLastTick);

    for (var i = 0; i < connectedPlayers.length; i++) { // Calculate physics for connected connectedPlayers
        connectedPlayers[i].velocity.x += connectedPlayers[i].acceleration.x * (timeSinceLastTick / 1000);
        if (connectedPlayers[i].velocity.x > 400) {
            connectedPlayers[i].velocity.x = 400;
        }
        connectedPlayers[i].x += connectedPlayers[i].velocity.x * (timeSinceLastTick / 1000);

        connectedPlayers[i].velocity.y += 900 * (timeSinceLastTick / 1000);
        if (connectedPlayers[i].velocity.y < 50 && connectedPlayers[i].velocity.y > -50) {
            connectedPlayers[i].velocity.y = 0;
        }
        if (connectedPlayers[i].velocity.y > 750) {
            connectedPlayers[i].velocity.y = 750;
        }
        connectedPlayers[i].y += connectedPlayers[i].velocity.y * (timeSinceLastTick / 1000);
    }

    lastUpdate = Date.now();
    setTimeout(serverTick, 16);
}