/* jshint esversion: 6 */

const localBaseURL = 'http://47.20.221.101:8080';
const herokuBaseURL = 'https://multiplayerphaser.herokuapp.com';

var baseURL = localBaseURL;

var config = {
	width: 1200,
	height: 1200,
	scale: {
		parent: 'phaserGame',
		mode: Phaser.Scale.FIT
	},
	physics: {
		default: 'arcade'
	}
};

var levelConfig = {
	preload: levelPreload,
	create: levelCreate,
	update: levelUpdate
};

// var mapConfig = {
// 	preload: mapPreload,
// 	create: mapCreate,
// 	update: mapUpdate
// };

// var menuConfig = {
// 	preload: menuPreload,
// 	create: menuCreate,
// 	update: menuUpdate
// };


window.onload = function () {
	game = new Phaser.Game(config);

	levelScene = game.scene.add('level', levelConfig);
	// mapScene = game.scene.add('map', mapConfig);
	// menuScene = game.scene.add('menu', menuConfig);

	game.scene.start('level', levelConfig);
}

function levelPreload() {
	this.load.spritesheet('player', 'assets/animatedDude.png', {
		frameWidth: 32,
		frameHeight: 48
	});
	this.load.image('tiles', 'assets/tileset.png');
	this.load.tilemapCSV('levelMap', 'assets/levelMap6.csv');
}

function levelCreate() {
	debug = this;

	map = this.make.tilemap({ key: 'levelMap', tileWidth: 64, tileHeight: 64 });
	tileset = map.addTilesetImage('tiles');
	layer = map.createStaticLayer(0, tileset, 0, 0);
	map.setCollisionBetween(0, 1000);

	player = this.add.sprite(32, 1200, 'player');
	this.physics.world.enable(player);
	player.scaleX = 1.5;
	player.scaleY = 1.5;
	player.body.gravity.y = 900;
	player.body.setBounce(0.3);
	player.body.setCollideWorldBounds(false);
	player.body.setAllowDrag(true);
	player.body.maxVelocity.x = 400;
	player.body.maxVelocity.y = 750;

	this.cameras.main.startFollow(player);
	this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

	this.anims.create({
		key: 'left',
		frames: this.anims.generateFrameNumbers('player', { start: 0, end: 3 }),
		frameRate: 10,
		repeat: -1
	});

	this.anims.create({
		key: 'straight',
		frames: [{ key: 'player', frame: 4 }],
		frameRate: 20
	});

	this.anims.create({
		key: 'right',
		frames: this.anims.generateFrameNumbers('player', { start: 5, end: 8 }),
		frameRate: 10,
		repeat: -1
	});

	attackButton = this.input.keyboard.addKey('SPACE');

	upButton = this.input.keyboard.addKey('W');

	leftButton = this.input.keyboard.addKey('A');
	leftButton.on('down', function () {
		player.anims.play('left');
	});

	rightButton = this.input.keyboard.addKey('D');
	rightButton.on('down', function () {
		player.anims.play('right');
	});


	accelerationText = this.add.text(10, 10, 'Acceleration: 0m/s^2');
	velocityXText = this.add.text(10, 30, 'X Velocity: 0m/s');
	velocityYText = this.add.text(10, 50, 'Y Velocity: 0m/s');

	pingText = this.add.text(1100, 10, 'PING: 0ms');
	serverConnectButton = this.add.text(1100, 30, 'CONNECT');
	serverConnectButton.setInteractive();
	serverConnectButton.on('pointerdown', function () {
		if (typeof playerId == 'undefined') {
			initConnection();
			serverConnectButton.text = 'DISCONNECT';
		} else {
			serverEnabled = !serverEnabled;
			if (serverEnabled) {
				serverConnectButton.text = 'DISCONNECT';
			} else {
				serverConnectButton.text = 'RECONNECT';
			}
		}
	});
}

frameNum = 0;
serverEnabled = false;

function levelUpdate() {
	frameNum++
	if (frameNum % 4 == 0 && serverEnabled) {
		updatePlayerVals();

		accelerationText.text = 'Acceleration: ' + player.body.acceleration.x + 'm/s^2';
		velocityXText.text = 'X Velocity: ' + Math.floor(player.body.velocity.x) + 'm/s';
		velocityYText.text = 'Y Velocity: ' + Math.floor(player.body.velocity.y) + 'm/s';
	}

	accelerationText.x = this.cameras.main.scrollX + 10;
	velocityXText.x = this.cameras.main.scrollX + 10;
	velocityYText.x = this.cameras.main.scrollX + 10;
	pingText.x = this.cameras.main.scrollX + 1100;
	serverConnectButton.x = this.cameras.main.scrollX + 1100;

	accelerationText.y = this.cameras.main.scrollY + 10;
	velocityXText.y = this.cameras.main.scrollY + 30;
	velocityYText.y = this.cameras.main.scrollY + 50;
	pingText.y = this.cameras.main.scrollY + 10;
	serverConnectButton.y = this.cameras.main.scrollY + 30;

	debug = this;

	this.physics.world.collide(layer, player);

	for (var i = 0; i < playerSprites.length; i++) {
		this.physics.world.collide(layer, playerSprites[i]);
	}

	// player movement code
	if (player.body.touching.down || player.body.blocked.down) {
		player.body.setDrag(3500, 0);

		if (leftButton.isDown) {
			player.body.acceleration.x = -1500;
		} else if (rightButton.isDown) {
			player.body.acceleration.x = 1500;
		} else {
			player.body.acceleration.x = 0;
			player.anims.play('straight');
		}

		if (upButton.isDown) {
			player.body.velocity.y -= 500;
		}
	} else if (!player.body.touching.down && !player.body.blocked.down) {
		player.body.setDrag(250, 0);

		if (leftButton.isDown) {
			player.body.acceleration.x = -500;
		} else if (rightButton.isDown) {
			player.body.acceleration.x = 500;
		} else {
			player.body.acceleration.x = 0;
			player.anims.play('straight');
		}
	}

	if (player.y > 2000) {
		player.x = 300;
		player.y = 500;
	}
}

var playerSprites = [];

function updatePlayerVals () {
	if (!serverEnabled) {
		return;
	}
	
	playerVals = {
		'id': playerId,
		'acceleration': {
			'x': player.body.acceleration.x,
			'y': player.body.acceleration.y
		},
		'velocity': {
			'x': player.body.velocity.x,
			'y': player.body.velocity.y
		},
		'x': player.x,
		'y': player.y,
		'inAir': true,
		'time': Date.now()
	};
	
	if (player.body.touching.down || player.body.blocked.down) {
		playerVals.inAir = false;
	}

	pingTime = Date.now();

	$.ajax({
		headers: {},
		url: baseURL + '/updatePlayerVals',
		type: "PUT",
		dataType: 'json',
		contentType: 'application/json',
		data: JSON.stringify(playerVals),
		success: function (res) {
			if (!serverEnabled) {
				return;
			}

			pongTime = Date.now();
			pingText.text = 'Ping: ' + String(pongTime - pingTime) + 'ms';

			incomingPlayerData = res;

			if (typeof incomingPlayerData == 'undefined') {
				return;
			}

			for (var i = 0; i < playerSprites.length; i++) { // Reset the data for each player sprite
				if (typeof playerSprites[i] != 'undefined') {
					playerSprites[i].data = null;
				}
			}

			for (var i = 0; i < incomingPlayerData.length; i++) {
				if (incomingPlayerData[i].id != playerId) {
					if (typeof playerSprites[i] == 'undefined') { // Create new player sprites if none exist to embody incoming player data
						playerSprites[i] = game.scene.scenes[0].add.sprite(32, 1000, 'player');
						game.scene.scenes[0].physics.world.enable(playerSprites[i]);
						playerSprites[i].scaleX = 1.5;
						playerSprites[i].scaleY = 1.5;
						playerSprites[i].body.setBounce(0);
						playerSprites[i].body.setCollideWorldBounds(false);
						playerSprites[i].body.setAllowDrag(true);
						playerSprites[i].body.maxVelocity.x = 400;
						playerSprites[i].body.maxVelocity.y = 750;
						playerSprites[i].body.gravity.y = 900;
						playerSprites[i].anims.play('straight');
					}

					playerSprites[i].data = incomingPlayerData[i].id;

					syncTween = game.scene.scenes[0].tweens.add({ // Animate the enemy in the same way as the player
						targets: playerSprites[i],
						x: incomingPlayerData[i].x,
						y: incomingPlayerData[i].y,
						ease: 'Linear',
						duration: 50,
						repeat: 0,
						yoyo: false
					});

					playerSprites[i].body.velocity.x = incomingPlayerData[i].velocity.x;
					playerSprites[i].body.velocity.y = incomingPlayerData[i].velocity.y;

					playerSprites[i].body.acceleration.x = incomingPlayerData[i].acceleration.x;
					playerSprites[i].body.acceleration.y = incomingPlayerData[i].acceleration.y;

					if (playerSprites[i].body.acceleration.x < 0) {
						if (playerSprites[i].anims.currentAnim.key != 'left') {
							playerSprites[i].anims.play('left');
						}
					} else if (playerSprites[i].body.acceleration.x > 0) {
						if (playerSprites[i].anims.currentAnim.key != 'right') {
							playerSprites[i].anims.play('right');
						}
					} else {
						if (playerSprites[i].anims.currentAnim.key != 'straight') {
							playerSprites[i].anims.play('straight');
						}
					}
				}
			}
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function initConnection() {
	pingTime = Date.now();

	$.ajax({
		headers: {},
		url: baseURL + '/getNewId',
		type: "GET",
		success: function (res) {
			playerId = res;

			pongTime = Date.now();
			pingText.text = 'Ping: ' + String(pongTime - pingTime) + 'ms';

			serverEnabled = true;
		},
		error: function (error) {
			console.error('Server refused to connect');
			console.log(error);
			pingText.text = 'Ping: ERROR';
		}
	});
}

function pingServer() {	
	pingTime = Date.now();

	$.ajax({
		headers: {},
		url: baseURL + '/ping',
		type: "GET",
		success: function () {
			pongTime = Date.now();
			pingText.text = 'Ping: ' + String(pongTime - pingTime) + 'ms';
		},
		error: function (error) {
			console.error('Server refused to connect');
			console.log(error);
			pingText.text = 'Ping: ERROR';
		}
	});
}

function resetServer() {
	$.ajax({
		headers: {},
		url: baseURL + '/resetServer',
		type: "GET",
		success: function () {
			console.log('Server reset');
		},
		error: function (error) {
			console.error('Server refused to connect');
			console.log(error);
			pingText.text = 'Ping: ERROR';
		}
	});
}